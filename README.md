# Plantuml4sql

This programs converts PlantUML diagrams from and to SQL database creation instructions.

It's primarily designed for Postgresql with the goal to synchronise documentation and database schema

A primary documentation (written in asciidoc) is available in doc directory

This package is compatible with Nuitka in order to create a binary file for your platform
