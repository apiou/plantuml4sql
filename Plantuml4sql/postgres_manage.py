import sys
import re
import time
from enum import Enum

from .datamodel import DataModel


def list_treat(state, elem, line):
    # let's treat a list of elements like ( val , val , val )
    # we suppose that elem is trimmed
    retry = True
    field_list = []
    while retry:
        retry = False
        if not elem:
            return state, field_list
        if state == 0:
            # beginning of the list. may encounter '(', '(val', or '(val)'
            if elem.startswith('('):
                elem = elem[1:]
                state = 1
            else:
                print("line %d " % line + "List does not start with '(' : " + elem)
                return -1, []
            retry = True
            continue
        if state == 1:
            # ok, we have the '(' let's treat the val
            state = 2
            idxc = elem.find(',')
            idxp = elem.find(')')
            idx = idxp if idxc < 0 else (idxc if idxp < 0 else min(idxp, idxc))
            if idx != -1:
                if idx == 0:
                    print("line %d " % line + " element is empty")
                    return -1, []
                field_list.append(elem[:idx])
                elem = elem[idx:]
                retry = True
                continue
            else:
                field_list.append(elem)
                return state, field_list
        if state == 2:
            # well, we had a val we now are waiting for ',' or ')'
            if elem.startswith(','):
                elem = elem[1:]
                state = 1
                retry = True
                continue
            elif elem == ')':
                return 3, field_list
            else:
                print("line %d " % line + " Malformed element list")
                return -1, []
    print("Invalid state")
    return -1, []


class Postgres:
    def __init__(self, filename=None):
        if filename == None:
            return
        try:  # Avoid exception on STDOUT
            with open(filename) as src:
                self.input = src.readlines()
                self.filename = filename
        except:
            print("Cannot open file: '" + filename + "'")
            sys.exit()

    def parse(self):
        # TODO: Treat simple views like: CREATE VIEW toto as select tab1.elem1 as tabelem1, tab2.elem2 as tabelem2 from tab1 tab2 where someting
        state = postgres_state.idle
        datamodel = DataModel(self.filename)
        primary = []
        index = []
        linenum = 0
        realcomment = None
        storedcomment = False  # if true, a comment is assigned to the current language element
        eol= False
        for line in self.input:
            line = line.strip()
            linenum += 1
            if not line:
                continue
            hascomment = False  # this is used to manage comment on one line
            for elem in line.split():
                # SQL is not really depend of line formatting and syntax elements may be spread on several lines
                # so treat each element individually only comments are line dependant
                if hascomment:
                    if comment is None:
                        comment = elem
                    else:
                        comment = comment + " " + elem
                    continue
                if elem == '--': # entering in comment section until end of line
                    hascomment = True
                    if storedcomment: # concatenate multiline comments if comments are not consumed
                        comment = comment + '\n'
                    else:
                        comment = None
                    continue

                if elem.upper() == 'CREATE' and state == postgres_state.idle:
                    # Treat CREATE ....
                    substate = 0
                    idx = 0
                    storedcomment = False
                    comment = None
                    state = postgres_state.create
                    continue
                if state == postgres_state.create:
                    if elem.upper() not in {'VIEW', 'TABLE', 'IF', 'NOT', 'EXISTS'}:
                        if substate == 1:
                            if elem.isidentifier():
                                obj.name = elem
                                state = postgres_state.createparopen
                            else:
                                print("line %d " % linenum + "Table or View name is not valid : " + elem)
                                break
                        else:
                            print("line %d " % linenum + "Unexpected token " + elem)
                    elif substate == 0 and elem.upper() == 'TABLE':
                        obj = DataModel.Table()
                        substate = 1
                    elif substate == 0 and elem.upper() == 'VIEW':
                        obj = DataModel.View()
                        substate = 1
                    continue
                if state == postgres_state.createparopen:
                    if elem == '(':
                        state = postgres_state.column
                        substate = -1
                        eol= False
                        col = {}
                        col["NOTNULL"] = False
                        col["UNIQUE"] = False
                        col["PRIMARY"] = False
                        col["FK"] = False
                        linetype = 'none'
                        continue
                if state == postgres_state.column:
                    # Treat column  or constraint definition in a table
                    if substate == -1:
                        if storedcomment:
                            obj.comment = comment
                            storedcomment = False
                        substate = 0
                    if substate < 97 and substate != 23 and substate != 26 and elem.endswith(',') or elem.endswith('),'):
                        # check that we have a final comma on elem only if we are not parsing a list of parameters unless we have a closing parenthesis before
                        elem = elem[:-1]
                        eol = True
                    if substate < 97 and elem.endswith(',)'):
                        elem = elem[:-2]
                        eol = True
                        state = postgres_state.parclose
                    if substate < 97 and elem.endswith(',);'):
                        elem = elem[:-3]
                        eol = True
                        state = postgres_state.endtable
                    if substate == 0 and elem == ')':
                        state = postgres_state.parclose
                        eol = True
                        continue
                    if substate == 0 and elem == ');':
                        state = postgres_state.endtable
                        eol = True
                        continue
                    if not elem:
                        continue
                    if substate == 0 and elem.upper() == 'CONSTRAINT':
                        substate = 20
                        continue
                    if substate == 0 and elem.upper() == 'UNIQUE':
                        list_state = 0
                        ids_list = []
                        substate = 23
                        linetype = elem.upper()
                        continue
                    if substate == 0 and elem.upper() in {'PRIMARY', 'FOREIGN'}:
                        substate = 22
                        linetype = elem.upper()
                        continue
                    if substate == 0 and elem.upper() in {'EXCLUDE', 'CHECK'}:
                        # these elements are ignored
                        break
                    if substate == 0:
                        # warning: check that all precedent matching elements with substate=0 continue
                        # here we have a column name
                        if elem.isidentifier():
                            col["NAME"] = elem
                            substate = 5
                            linetype = 'COL'
                            continue
                        else:
                            print("line %d " % linenum + "Column name is not valid : " + elem)
                            break
                    if substate == 5:
                        # get type until a known keyword is discovered
                        if elem.upper() not in (
                        'CONSTRAINT', 'NOT', 'NULL', 'UNIQUE', 'PRIMARY', 'REFERENCES', 'DEFAULT'):
                            if "type" not in col:
                                col["TYPE"] = elem.lower()
                            else:
                                col["TYPE"] = col["TYPE"] + " " + elem.lower()
                            continue
                        else:
                            substate = 10  # fallthrough
                    if substate >= 10 and substate < 20:
                        # treat elements of a column
                        if elem.upper() == 'NOT' and substate == 10:
                            substate = 11
                        elif elem.upper() == 'NULL':
                            if substate == 11:
                                col["NOTNULL"] = True
                            substate = 10
                        elif elem.upper() == 'UNIQUE':
                            col["UNIQUE"] = True
                        elif elem.upper() == 'PRIMARY' and substate == 10:
                            substate = 12
                        elif elem.upper() == 'KEY' and substate == 12:
                            col["PRIMARY"] = True
                            substate = 10
                        elif elem.upper() == 'DEFAULT' and substate == 10:
                            substate = 13
                        elif substate == 13:
                            if "DEFAULT" in col:
                                col["DEFAULT"] = col["DEFAULT"] + " " + elem
                            else:
                                col["DEFAULT"] = elem
                        continue
                    if substate >= 20 and substate < 30:
                        # treat table constraints
                        if substate == 20:
                            # here we have constraint name. Ignore it
                            substate = 21
                            continue
                        if substate == 21 and elem.upper() == 'UNIQUE':
                            list_state = 0
                            ids_list = []
                            substate = 23
                            linetype = elem.upper()
                            continue
                        if substate == 21 and elem.upper() in {'PRIMARY', 'FOREIGN'}:
                            substate = 22
                            linetype = elem.upper()
                            continue
                        if substate == 21 and elem.upper() in {'EXCLUDE', 'CHECK'}:
                            # these elements are ignored
                            break
                        if substate == 22:
                            if elem.upper() == 'KEY':
                                list_state = 0
                                ids_list = []
                                substate = 23
                            else:
                                print("line %d " % linenum + "Primary or Foreign not followed by Key : " + elem)
                                break
                            continue
                        if substate == 23:
                            (list_state, elements) = list_treat(list_state, elem, linenum)
                            ids_list = ids_list + elements
                            if list_state == -1:
                                break
                            if list_state == 3:
                                if linetype == 'FOREIGN':
                                    substate = 24
                                else:
                                    substate = 0
                            continue
                        if substate == 24:
                            if elem.upper() == 'REFERENCES':
                                substate = 25
                                continue
                            else:
                                print("line %d " % linenum + "Foreign Key not followed by REFERENCES : " + elem)
                                break
                        if substate == 25:
                            if elem.isidentifier():
                                fktable = elem
                                list_state = 0
                                cols_list = []
                                substate = 26
                                continue
                            else:
                                print("line %d " % linenum + "Table name is not valid : " + elem)
                                break
                        if substate == 26:
                            (list_state, elements) = list_treat(list_state, elem, linenum)
                            cols_list = cols_list + elements
                            if list_state == -1:
                                break
                            if list_state == 3:
                                substate = 27
                                dcascade = False
                                ucascade = False
                            continue
                        if substate == 27:
                            if elem == 'ON':
                                substate = 28
                            continue
                        if substate == 28:
                            if elem in {'DELETE', 'UPDATE'}:
                                substate = 29
                                casctype = elem
                            continue
                        if substate == 29:
                            if elem == "CASCADE":
                                if casctype == 'DELETE':
                                    dcascade = True
                                elif casctype == 'UPDATE':
                                    ucascade = True
                                substate = 27
                            continue
                        print("line %d " % linenum + "Unknown syntax: " + elem)
                        break
                if state == postgres_state.parclose:
                    # waiting for end of instruction
                    keywords = line.split()
                    for elem in keywords:
                        if elem == ';':
                            state = postgres_state.endtable
                        else:
                            print("line %d " % linenum + "Unknown syntax: " + elem)
                            break;
                if line.upper().startswith('ALTER') and state == postgres_state.idle:
                    # Treat ALTER ....
                    substate = 0
                    storedcomment = False
                    comment = None
                    state = postgres_state.alter
                    continue
                if state == postgres_state.alter:
                    if elem.endswith(';'):
                        state = postgres_state.alterend
                        elem = elem[:-1]
                        eol = True
                    if not elem:
                        continue
                    if substate == 0 and elem.upper() == 'TABLE':
                        substate = 1
                        continue
                    elif substate == 1:
                        if elem.isidentifier():
                            name = elem
                            substate = 2
                            continue
                        else:
                            state = postgres_state.idle
                            print("line %d " % linenum + "Table name is not valid : " + elem)
                            break
                    elif substate == 2 and elem.upper() == 'ADD':
                        substate = 3
                        continue
                    elif substate == 3 and elem.upper() == 'CONSTRAINT':
                        substate = 4
                        continue
                    elif substate == 4:
                        if not elem.isidentifier():
                            print("line %d " % linenum + "Table name is not valid : " + elem)
                            break
                        else:
                            substate = 5
                            continue
                    elif substate == 5 and elem.upper() in {'PRIMARY', 'FOREIGN'}:
                        substate = 6
                        linetype = elem.upper()
                        continue
                    elif substate == 5 and elem.upper() in {'UNIQUE'}:
                        substate = 7
                        linetype = elem.upper()
                        continue
                    elif substate == 5 and elem.upper() in {'EXCLUDE', 'CHECK'}:
                        # these elements are ignored
                        break
                    elif substate == 6:
                        if elem.upper() == 'KEY':
                            list_state = 0
                            ids_list = []
                            substate = 7
                        else:
                            print("line %d " % linenum + "Primary or Foreign not followed by Key : " + elem)
                            break
                        continue
                    elif substate == 7:
                        (list_state, elements) = list_treat(list_state, elem, linenum)
                        ids_list = ids_list + elements
                        if list_state == -1:
                            break
                        if list_state == 3:
                            if linetype == 'FOREIGN':
                                substate = 8
                            else:
                                substate = 0
                        continue
                    elif substate == 8:
                        if elem.upper() == 'REFERENCES':
                            substate = 9
                            continue
                        else:
                            print("line %d " % linenum + "Foreign Key not followed by REFERENCES : " + elem)
                            break
                    elif substate == 9:
                        if elem.isidentifier():
                            fktable = elem
                            list_state = 0
                            cols_list = []
                            substate = 10
                            continue
                        else:
                            print("line %d " % linenum + "Table name is not valid : " + elem)
                            break
                    elif substate == 10:
                        (list_state, elements) = list_treat(list_state, elem, linenum)
                        cols_list = cols_list + elements
                        if list_state == -1:
                            break
                        if list_state == 3:
                            substate = 11
                            dcascade = False
                            ucascade = False
                        continue
                    elif substate == 11:
                            if elem == 'ON':
                                substate = 12
                            continue
                    if substate == 12:
                        if elem in {'DELETE', 'UPDATE'}:
                            substate = 13
                            casctype = elem
                        continue
                    if substate == 13:
                        if elem == "CASCADE":
                            if casctype == 'DELETE':
                                dcascade = True
                            elif casctype == 'UPDATE':
                                ucascade = True
                            substate = 11
                        continue
                    print("line %d " % linenum + "Unknown syntax: " + elem)
                    break
                if line.upper().startswith('COMMENT') and state == postgres_state.idle:
                    # Treat COMMENT ....
                    substate = 0
                    storedcomment = False
                    comment = None
                    realcomment = None
                    state = postgres_state.comment
                    continue
                if state == postgres_state.comment:
                    if elem.endswith(';'):
                        state = postgres_state.commentend
                        elem = elem[:-1]
                        eol = True
                    if not elem:
                        continue
                    if substate == 0 and elem.upper() == 'ON':
                        substate = 1
                        continue
                    elif substate == 1 and elem.upper() == 'TABLE':
                        substate = 2
                        linetype = 'TABLE'
                        continue
                    elif substate == 1 and elem.upper() == 'COLUMN':
                        substate = 7
                        linetype = 'COLUMN'
                        continue
                    elif substate == 2:
                        if elem.isidentifier():
                            name = elem
                            substate = 3
                            continue
                        else:
                            state = postgres_state.idle
                            print("line %d " % linenum + "Table name is not valid : " + elem)
                            break
                    elif substate == 3 and elem.upper() == 'IS':
                        substate = 4
                        continue
                    elif substate == 4:
                        if realcomment == None:
                            if elem.startswith("'"):
                                elem = elem[1:]
                            else:
                                substate = 5
                            realcomment = elem
                        else:
                            if elem.endswith("'"):
                                elem = elem[:-1]
                                substate = 5
                            realcomment = realcomment + " " + elem
                        continue
                    elif substate == 5:
                        state = postgres_state.idle
                        print("line %d " % linenum + "comment is finished without ; after " + elem)
                        break
                    elif substate == 7:
                        ids = elem.split('.')
                        if len(ids) != 2:
                            state = postgres_state.idle
                            print("line %d " % linenum + "For a Comment, a column name is identified by table.column. We have :" + elem)
                            break
                        name = ids[0]
                        column = ids[1]
                        substate = 3
                        continue
            if hascomment:
                storedcomment = True
            if eol and ( state == postgres_state.column or state == postgres_state.endtable or state == postgres_state.alterend or state == postgres_state.commentend):
                # Lastelement contains a , we store the parsed object
                if state == postgres_state.column or state == postgres_state.endtable:
                    if linetype == 'COL':
                        if storedcomment:
                            col["COMMENT"] = comment
                        obj.cols.append(col)
                    if linetype == 'PRIMARY':
                        obj.set_primary(ids_list)
                    if linetype == 'UNIQUE':
                        obj.set_unique(ids_list)
                    if linetype == 'FOREIGN':
                        if not storedcomment:
                            comment=""
                        obj.set_foreign(ids_list, fktable, cols_list, dcascade, ucascade, comment)
                if state == postgres_state.endtable:
                    # well, we have a table or a view
                    # store it
                    if obj != None:
                        datamodel.tables.append(obj)
                    state = postgres_state.idle
                if state == postgres_state.alterend:
                    if linetype == 'UNIQUE':
                        datamodel.tables_alterunique(name, ids_list)
                    if linetype == 'PRIMARY':
                        datamodel.tables_alterprimary(name, ids_list)
                    if linetype == 'FOREIGN':
                        if not storedcomment:
                            comment=""
                        datamodel.tables_alterforeign(name, ids_list, fktable, cols_list, dcascade, ucascade, comment)
                    state = postgres_state.idle
                if state == postgres_state.commentend:
                    if linetype == 'TABLE':
                        datamodel.tables_comment(name, realcomment)
                    if linetype == 'COLUMN':
                        datamodel.tables_commentcol(name, column, realcomment)
                    state = postgres_state.idle
                substate = 0
                eol= False
                col = {}
                col["NOTNULL"] = False
                col["UNIQUE"] = False
                col["PRIMARY"] = False
                col["FK"] = False
                storedcomment= False
                linetype = 'none'

        return datamodel

    def write(self, datamodel, outfile):
       with open(outfile, "w") as ofile:
            print("-- Database created on", time.strftime('%d/%m/%y %H:%M', time.localtime()), "from", datamodel.filename, file=ofile)
            # print tables
            for table in datamodel.tables:
                print('table("%s") {' % table.name, file=ofile)
                print('desc("%s")' % table.comment.replace('\n', '\\n'), file=ofile)
                field_list = []
                for col in table.cols:
                    col_type = "field"
                    if col["PRIMARY"] == True:
                        col_type = "primary_key"
                    elif col["UNIQUE"] == True:
                        col_type = "unique_key"
                    elif col["FK"] == True:
                        col_type = "foreign_key"
                    if col["NOTNULL"] == True:
                        type = col["TYPE"] + " NOT NULL"
                    else:
                        type = col["TYPE"]
                    if "COMMENT" in col:
                        comment = col["COMMENT"].replace('\n', '\\n')
                    else:
                        comment = None
                    if col_type == "field":
                        field_list.append([col_type, col["NAME"], type, comment])
                    else:
                        if comment == None:
                            print('%s("%s", "%s")' % (col_type, col["NAME"], type), file=ofile)
                        else:
                            print('%s("%s", "%s", "%s")' % (col_type, col["NAME"], type, comment), file=ofile)
                if len(field_list) != 0:
                    print("..", file=ofile)
                    for field in field_list:
                        if field[3] == None:
                            print('%s("%s", "%s")' % (field[0], field[1], field[2]), file=ofile)
                        else:
                            print('%s("%s", "%s", "%s")' % (field[0], field[1], field[2], field[3]), file=ofile)
                print("}\n", file=ofile)
            # print relations
            for table in datamodel.tables:
                for fk in table.fk:
                    fkcount = 0
                    fkpcount = 0
                    pkcount = 0
                    nulavail = True
                    for col in table.cols:
                        if col["PRIMARY"]:
                            pkcount += 1
                        for fkcol in fk["COLS"]:
                            if col["NAME"] == fkcol:
                                if col["PRIMARY"] or (col["NOTNULL"] and col["UNIQUE"]):
                                    fkcount += 1
                                if col["PRIMARY"]:
                                    fkpcount += 1
                                if col["NOTNULL"]:
                                    nulavail = False
                    if pkcount > fkpcount:
                        if nulavail:
                            type = "reln0"
                        else:
                            type = "reln1"
                    elif pkcount == fkpcount:
                        type = "rel01"
                    print("%s(%s, %s)" % (type, table.name, fk["FOREIGN_TABLE"]), file=ofile)

            print("@enduml", file=ofile)


class postgres_state(Enum):
    idle = 0,
    create = 1,
    createparopen = 2,
    column = 4,
    parclose = 5,
    endtable = 6,
    alter = 7,
    alterend = 8,
    comment = 9,
    commentend = 10,
