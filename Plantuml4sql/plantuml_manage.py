import sys
import re
import time
from enum import Enum
from .datamodel import DataModel

# PlantUML allows some HTML tags in comments.
# We don't want them anymore here...

TAG_RE = re.compile(r'<[^>]+>')
TAG_SEPM = re.compile(r'^[\-]+$')
TAG_SEPD = re.compile(r'^[\.]+$')
TAG_SEPE = re.compile(r'^[\=]+$')

def strip_html_tags(t):
    return TAG_RE.sub('', t)

class Plantuml:
    def __init(self, filename = None):
        if filename == None:
            return
        try: # Avoid exception on STDOUT
            with open(filename) as src:
                self.input = src.readlines()
                self.filename = filename
        except:
            print("Cannot open file: '" + filename + "'")
            sys.exit()

    def parse(self):
        state = plantuml_state.enduml
        datamodel = DataModel(self.filename)
        primary = []
        index = []
        linenum= 0
        for line in self.input:
            line = line.strip()
            linenum += 1
            if not line:
                continue
            if line == "@startuml":
                if state != plantuml_state.enduml:
                    print("@startuml encountered at line " + linenum + " in " + self.filename)
                state = plantuml_state.startuml
                continue
            if state != plantuml_state.startuml:
                continue
            comment = ""
            i = line.split()
            fname = i[0]
            if fname == ".." or fname == "__": # Separators in table definition
                continue
            if state == plantuml_state.table_field and ("--" in line):
                i, comment = line.split("--", 2)
                i = i.split()
            pk = False; idx = False
            if fname[0] in ("+", "#"):
                if fname[0] == "#":
                    pk = True
                else:
                    idx = True
                fname = fname[1:]
            if line == "@enduml":
                state = plantuml_state.enduml
                continue
            if state == plantuml_state.enduml:
                 continue
            if line.startswith("class"):
                state = plantuml_state.table
                primary = []; index = ""
                # Table names are quoted and lower cased to avoid conflict with a mySQL reserved word
                print("CREATE TABLE IF NOT EXISTS `" + i[1].lower() + "` (")
                continue
            if state == plantuml_state.table and line == "==": # Separator after table description
                state = plantuml_state.table_field
                continue
            if state == plantuml_state.table_field and line == "}":
                state = plantuml_state.startuml
                print("  PRIMARY KEY (%s)" % ", ".join(primary), end="")
                if index:
                    print(",\n%s" % index[:-2],)
                    index = ""
                print(");\n")
                continue
            if state == plantuml_state.table_field and line == "#id":
                print("  %-16s SERIAL," % "id")
            if state == plantuml_state.table_field and line != "#id":
                print("  %-16s %s" % (fname, " ".join(i[2:]).upper()), end="")
                if comment:
                    # Avoid conflict with apostrophes (use double quotation marks)
                    print(" COMMENT \"%s\"" % strip_html_tags(comment.strip()), end="")
                print(",")
            if state == plantuml_state.table_field and pk:
                primary.append(fname)
            if state == plantuml_state.table_field and idx:
                index += "  INDEX (%s),\n" % fname

    def write(self, datamodel, outfile):
        with open(outfile, "w") as ofile:
            print("@startuml", file=ofile)
            print("' model created on ", time.strftime('%d/%m/%y %H:%M', time.localtime()), "from", datamodel.filename, file=ofile)
            print("""
!define table(x) class x << (T,orchid) >>
!define view(x) class x << (V, orange) >>
!definelong desc(x)
x
___
!enddefinelong
!define primary_key(name, type, comment) + <u><b>name</b></u> : type  -- comment
!define primary_key(name, type) + <u><b>name</b></u> : type
!define foreign_key(name, foreign) # <b>name</b> is foreign
!define foreign_key(name, foreign, comment) # <b>name</b> is foreign  -- comment
!define unique_key(name, type, comment) ~ name : type  -- comment
!define unique_key(name, type) ~ name : type
!define field(name, type, comment) name : type  -- comment
!define field(name, type) name : type
!define reln0(enta,entb)  enta "n" }o--o| "0" entb
!define reln1(enta,entb)  enta "n" }|--|| "1" entb
!define rel11(enta,entb)  enta "1" ||--|| "1" entb
!define rel01(enta,entb)  enta "0" |o--|| "1" entb
!define rel10(enta,entb)  enta "1" ||--o| "0" entb
!define rel1n(enta,entb)  enta "1" ||--|{ "n" entb
!define rel0n(enta,entb)  enta "0" |o--o{ "n" entb

hide methods
hide stereotypes
""", file=ofile)
            # print tables
            for table in datamodel.tables:
                print('table("%s") {' % table.name, file=ofile)
                print('desc("%s")' % table.comment.replace('\n', '\\n'), file=ofile)
                field_list = []
                for col in table.cols:
                    col_type = "field"
                    if col["PRIMARY"] == True:
                        col_type = "primary_key"
                    elif col["UNIQUE"] == True:
                        col_type = "unique_key"
                    elif col["FK"] == True:
                        col_type = "foreign_key"
                    if col["NOTNULL"] == True:
                        type = col["TYPE"] + " NOT NULL"
                    else:
                        type = col["TYPE"]
                    if "COMMENT" in col:
                        comment = col["COMMENT"].replace('\n', '\\n')
                    else:
                        comment = None
                    if col_type == "field":
                        field_list.append([ col_type, col["NAME"], type, comment])
                    else:
                        if comment == None:
                            print( '%s("%s", "%s")' % (col_type, col["NAME"], type), file=ofile)
                        else:
                            print( '%s("%s", "%s", "%s")' % (col_type, col["NAME"], type, comment), file=ofile)
                if len(field_list) != 0:
                    print("..", file=ofile)
                    for field in field_list:
                        if field[3] == None:
                            print( '%s("%s", "%s")' % ( field[0], field[1], field[2]) , file=ofile)
                        else:
                            print( '%s("%s", "%s", "%s")' % ( field[0], field[1], field[2], field[3]) , file=ofile)
                print("}\n", file=ofile)
            # print relations
            for table in datamodel.tables:
                for fk in table.fk:
                    fkcount = 0
                    fkpcount = 0
                    pkcount = 0
                    nulavail = True
                    for col in table.cols:
                        if col["PRIMARY"]:
                            pkcount +=1
                        for fkcol in fk["COLS"]:
                            if col["NAME"] == fkcol:
                                if col["PRIMARY"] or (col["NOTNULL"] and col["UNIQUE"]):
                                    fkcount += 1
                                if col["PRIMARY"]:
                                    fkpcount += 1
                                if col["NOTNULL"]:
                                    nulavail = False
                    if pkcount > fkpcount:
                        if nulavail:
                            type = "reln0"
                        else:
                            type = "reln1"
                    elif pkcount == fkpcount:
                            type = "rel01"
                    print ("%s(%s, %s)" % ( type, table.name, fk["FOREIGN_TABLE"]), file=ofile)

            print("@enduml", file=ofile)


class plantuml_state(Enum):
    start=0,
    startuml=1,
    enduml=2,
    table=3,
    table_field=4,
    fkey=5
    view=6,
    view_field=7,
    view_link=8
