#
# Copyright 2019 Stephane Apiou
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

import toml


class config_manage():
    default_config = {
        'application': {
            'database' : 'postgresql',
            'erd' : 'plantuml'
        },
        'internal': {
            'some_property1': True
        }
    }

    def __init__(self, app_name):
        """ On init, read the saved configuration file if exists which is stored in $HOME directory as .(app_name)
        if the file does not exists, the default configuration defined in this class is used.
        special cases:
        * if a parameter is defined in internal section, this parameter is never exported or defined if not explicitly
        defined by the user or the application. Most of the time this is used for internal purpose, debug or non disclosed
        functionalities
        * if a parameter is defined as <>, it's considered as a template parameter. It's convenient if for example you have to configure
        several items with the same structure. for example: account definitions, remote servers definitions ...
        Conformity of each element (section, setting, subsetting and type) is compared with the default configuration. If not compatible, the value is rejected silently
        TODO: No version management of the configuration file implemented so far.
        :param app_name:  the name of the application. this is used to define the name of parameter file
        :type app_name: str
        """

        self.conf_dir = os.path.expanduser("~/.config")
        self.conf_file = os.path.join(self.conf_dir, app_name + "rc")

        if os.path.isfile(self.conf_file):
            readconfig = toml.load(self.conf_file)
        else:
            readconfig = dict()

        # normalize configuration
        # or create a new config structure based on default_config structure
        self.config = dict()
        for section, settings in self.default_config.copy().items():
            newsection = dict()
            readsetting = readconfig.get(section)
            if readsetting is None or type(readsetting) is not dict:
                if section != 'internal':
                    if "<>" in settings:
                        self.config[section] = dict()
                    else:
                        self.config[section] = settings
            else:
                for setting, value in settings.items():
                    if (setting == "<>"):
                        if len(settings) == 1:
                            for elem, params in readsetting.items():
                                newsubsection = dict()
                                for subsetting, subvalue in value.items():
                                    readsubvalue = params.get(subsetting)
                                    if readsubvalue is not None and type(readsubvalue) is type(subvalue):
                                        newsubsection[subsetting] = readsubvalue
                                    else:
                                        if section != 'internal':
                                            newsubsection[subsetting] = subvalue
                                if len(newsubsection):
                                    newsection[elem] = newsubsection
                    else:
                        readvalue = readsetting.get(setting)
                        if readvalue is not None and type(readvalue) is type(value):
                            newsection[setting] = readvalue
                        else:
                            if section != 'internal':
                                newsection[setting] = value
            if len(newsection):
                self.config[section] = newsection

    def get_default(self, section, setting, subsetting = None):
        """ Get default value for a setting of a section. if the setting is a dictionary, the dictionary is returned.
        You may indicated a subsetting in order to obtain the value from the dictionary stored in the setting
        :param section:  is the section of settings
        :type section: str
        :param setting: a setting, it may includes a subsetting
        :type setting: str
        :param subsetting: a subsetting of a setting this is optionnal as most parameters are defined only in settings. Subsetting is supposed to be used ONLY for template settings.
        :type subsetting: str

        :return: the default value stored in the setting or subsetting if the subsetting is given
        :rtype: Any
        """
        if section in self.default_config:
            ref_setting = self.default_config[section]
            if len(ref_setting) == 1 and "<>" in ref_setting:
                if subsetting is None:
                    return ref_setting["<>"]
                elif subsetting in ref_setting["<>"]:
                    return ref_setting["<>"][subsetting]
            elif setting in ref_setting:
                if subsetting is None:
                    return ref_setting[setting]
                elif subsetting in ref_setting[setting]:
                    return ref_setting[setting][subsetting]
        raise config_exception("Cannot get section: "+ section + " or setting: "+setting+ " or subsetting: "+subsetting)

    def get(self, section, setting, subsetting = None):
        """ get the value of a dedicated item. If item is not defined, get the default value
         You may indicated a subsetting in order to obtain the value from the dictionary stored in the setting
        :param section:  is the section of settings
        :type section: str
        :param setting: a setting, it may includes a subsetting
        :type setting: str
        :param subsetting: a subsetting of a setting this is optionnal as most parameters are defined only in settings. Subsetting is supposed to be used ONLY for template settings.
        :type subsetting: str

        :return: the value stored in the setting or subsetting if the subsetting is given. If no value is stored, the default value is returned
        :rtype: Any
        """
        if section in self.default_config:
            ref_setting = self.default_config[section]
            if section not in self.config:
                return self.get_default(section, setting, subsetting)
            if len(ref_setting) == 1 and "<>" in ref_setting:
                if subsetting is None:
                    if setting in self.config[section]:
                        return self.config[section][setting]
                elif setting in self.config[section]:
                    if subsetting in self.default_config[section]["<>"]:
                        if subsetting in self.config[section][setting]:
                            return self.config[section][setting][subsetting]
                        else:
                            return self.default_config[section]["<>"][subsetting]
            elif setting in ref_setting:
                if subsetting is None:
                    if setting in self.config[section]:
                        return self.config[section][setting]
                    else:
                        return self.default_config[section][setting]
                elif subsetting in ref_setting[setting]:
                    if setting in self.config[section]:
                        if subsetting in self.config[section][setting]:
                            return self.config[section][setting][subsetting]
                        else:
                            return self.default_config[section][setting][subsetting]
                    else:
                        return self.default_config[section][setting][subsetting]
        raise config_exception("Cannot get section: "+ section + " or setting: "+setting)

    # write
    def set_modify(self, value, section, setting, subsetting=None):
        """ Set or Modify a setting from a section with the value given.

        :param value:
        :param section:  is the section of settings
        :type section: str
        :param setting: a setting, it may includes a subsetting
        :type setting: str
        :param subsetting: a subsetting of a setting this is optionnal as most parameters are defined only in settings. Subsetting is supposed to be used ONLY for template settings.
        :type subsetting: str
        :param section:
        :param setting:
        :param subsetting:
        :return: Nothing
        """
        if section in self.default_config:
            ref_setting = self.default_config[section]
            if len(ref_setting) == 1 and "<>" in ref_setting:
                if subsetting is None:
                    if type(value) is type(ref_setting["<>"]):
                        self.config[section][setting] = value
                        return
                elif subsetting in ref_setting["<>"] and type(value) is type(ref_setting["<>"][subsetting]):
                    self.config[section][setting][subsetting] = value
                    return
            elif setting in ref_setting:
                if subsetting is None:
                    if type(value) is type(ref_setting[setting]):
                        self.config[section][setting] = value
                        return
                elif subsetting in ref_setting[setting] and type(value) is type(ref_setting[setting][subsetting]):
                    self.config[section][setting][subsetting] = value
                    return
        raise config_exception("Cannot add or modify section: "+ section + " or setting: "+setting)

    def set_default(self, section, setting, subsetting=None):
        """ Set the default value for a setting from a section. This is particularly useful for a setting based on a template in order to define the default values

        :param section:  is the section of settings
        :type section: str
        :param setting: a setting, it may includes a subsetting
        :type setting: str
        :param subsetting: a subsetting of a setting this is optionnal as most parameters are defined only in settings. Subsetting is supposed to be used ONLY for template settings.
        :type subsetting: str
        :return: nothing
        """
        self.set_modify(self.get_default(section, setting, subsetting), setting, subsetting)

    def has(self, section, setting, subsetting=None):
        """ Check if the setting of a section is defined or not.
        This is particularly useful for sections defined by a template
        :raise config_exception: if the section and setting does not exists

        :param section:  is the section of settings
        :type section: str
        :param setting: a setting, it may includes a subsetting
        :type setting: str
        :param subsetting: a subsetting of a setting this is optionnal as most parameters are defined only in settings. Subsetting is supposed to be used ONLY for template settings.
        :type subsetting: str
        :return:  True if the section and setting is defined, False if only the default configuration exists
        :rtype: bool
        """
        if section in self.default_config:
            ref_setting = self.default_config[section]
            if section not in self.config:
                return False
            if len(ref_setting) == 1 and "<>" in ref_setting:
                if subsetting is None:
                    if setting in self.config[section]:
                        return True
                    else:
                        return False
                elif setting in self.config[section]:
                    if subsetting in self.default_config[section][setting]:
                        if subsetting in self.config[section][setting]:
                            return True
                        else:
                            return False
                else:
                    return False
            elif setting in ref_setting:
                if setting in self.config[section]:
                    return True
                else:
                    return False
        raise config_exception("Cannot get section: "+ section + " or setting: "+setting)

    def delete(self, section, setting):
        """ delete a specific setting of a section. This is useful when a setting is defined based on a template setting
        :raise config_exception: if the section and setting is not defined
        :param section:  is the section of settings
        :type section: str
        :param setting: a setting, it may includes a subsetting
        :type setting: str
        :return: nothing
        """
        if section in self.default_config:
            ref_setting = self.default_config[section]
            if len(ref_setting) == 1 and "<>" in ref_setting:
                del self.config[section][setting]
                return
        raise config_exception("Cannot delete section: "+ section+ " or setting: "+setting)

    def write(self):
        """ Write the configuration stored in the class as a toml file
        :raise IOError:
        :return:  nothing.
        """
        if not os.path.exists(self.conf_dir):
            os.makedirs(self.conf_dir)
        serialized = toml.dumps(self.config)
        with open(self.conf_file, 'w') as confFileid:
            confFileid.write(serialized)


class config_exception(Exception):
    """base config exception"""
