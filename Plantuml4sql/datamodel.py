# define the datamodel.
# So far, we only support views and tables
# Tables describes:
#   table name and comment for the table
#   rows: name with type (no filtering of data type) attributes: unique key, index, not null and a comment
#   foreign key to another table (control is performed)
#
# views describes:
#   view name and comment for the table
#   rows: name of row and related row from a table
#   constraints: constraints to define view.
#   remark: complex requests are not well mapped to plantuml and reversely, it's not possible to describe complex requests in plantuml


class DataModel:
    def __init__(self, filename):
        self.filename = filename # from filename for future use
        self.tables = []
        self.views = []

    def tables_alterunique(self,tablename, keys):
        for table in self.tables:
            if table.name == tablename:
                table.set_unique(keys)
                break

    def tables_alterprimary(self,tablename, keys):
        for table in self.tables:
            if table.name == tablename:
                table.set_primary(keys)
                break

    def tables_alterforeign(self,tablename, elements, fktable, cols_list, dcascade, ucascade, comment):
        for table in self.tables:
            if table.name == tablename:
                table.set_foreign(elements, fktable, cols_list, dcascade, ucascade, comment)
                break

    def tables_comment(self,tablename, comment):
        for table in self.tables:
            if table.name == tablename:
                table.comment =comment
                break

    def tables_commentcol(self,tablename, column, comment):
        for table in self.tables:
            if table.name == tablename:
                for col in table.cols:
                    if col["NAME"] == column:
                        col["COMMENT"] = comment
                        break
                break

    class Table:
        def __init__(self, name='',comment=''):
            self.name = name
            self.comment = comment
            self.cols = []
            self.fk = []

        def set_primary(self, keys):
            for key in keys:
                for col in self.cols:
                    if col["NAME"] == key:
                        col["PRIMARY"] = True
                        break

        def set_unique(self, keys):
            for key in keys:
                for col in self.cols:
                    if col["NAME"] == key:
                        col["UNIQUE"] = True
                        break

        def set_foreign(self,elements, fktable, cols_list, dcascade, ucascade, comment):
            for key in elements:
                for col in self.cols:
                    if col["NAME"] == key:
                        col["FK"] = True
                        break
            self.fk.append({"COLS": elements, "FOREIGN_TABLE" : fktable, "FOREIGN_COLS": cols_list, "DCASCADE" : dcascade, 'UCASCADE': ucascade, "COMMENT" : comment})


    class View:
        def __init__(self, name='',comment=''):
            self.name = name
            self.comment = comment
            self.cols = []
            self.refs = []
            self.constraints = []
