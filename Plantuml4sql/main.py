# main.py
#
# Copyright 2019 Stephane Apiou
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os

from .plantuml_manage import Plantuml
from .postgres_manage import Postgres
from .config_manage import config_manage
from .arg_parser import arg_parser

def main(version):
    Params = sys.argv

    conf = config_manage('plantuml4sql')
    parser = arg_parser()
    args = parser.parse_args(Params)

    if args.version == True:
        print("plantuml4sql version "+ version)
        return 0

    if args.database == None:
        args.database= conf.get("application", "database")
    db_ext=".sql"
    if args.erd == None:
        args.erd= conf.get("application", "erd")
    if args.erd == "plantuml":
        erd_ext = ".puml"
    if args.outfile == None:
        file=  os.path.splitext(args.infile[0])
        if file[1] in (db_ext, erd_ext) or file[1] == "":
            if args.toerd:
                infile = file[0] + db_ext
            else:
                infile = file[0] + erd_ext
        else:
            infile = args.infile[0]
        if args.toerd:
            outfile = file[0] + erd_ext
        elif args.tosql:
            outfile = file[0] + db_ext
    else:
        infile = args.infile[0]
        outfile = args.outfile

    if args.toerd:
        if args.database == "postgresql":
            db = Postgres(infile)
            model = db.parse()
        else:
            parser.print_help()
            return 0
        if args.erd == "plantuml":
            erd = Plantuml()
            erd.write(model, outfile)
        else:
            parser.print_help()
            return 0
    elif args.tosql:
        if args.database == "plantuml":
            erd = Plantuml(infile)
            model = erd.parse()
        else:
            parser.print_help()
            return 0
        if args.database == "postgresql":
            db = Postgres()
            db.write(model, outfile)
        else:
            parser.print_help()
            return 0
    else:
        parser.print_help()
        return 0
    conf.write()
    return 0
