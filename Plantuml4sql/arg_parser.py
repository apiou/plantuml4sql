#
# Copyright 2019 Stephane Apiou
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse

class arg_parser():
    def __init__(self):
        self.parser = argparse.ArgumentParser(description="Converts plantuml objects from or to sql instructions")
        self.parser.add_argument('-v', '--version', help='Print version and exit', action='store_true')
        group = self.parser.add_mutually_exclusive_group()
        group.add_argument('-s', '--tosql', help='convert file to sql', action='store_true')
        group.add_argument('-p', '--toerd', help='print sql in erd', action='store_true')
        self.parser.add_argument('-d', '--database', help='Define database type', nargs=1, metavar='<database_type>')
        self.parser.add_argument('-e', '--erd', help='Define ERD tool', nargs=1, metavar='<database_type>')
        self.parser.add_argument('infile', help='file on which the operation is performed', nargs=1)
        self.parser.add_argument('outfile', help='output file: deduced from infile if not specified ', nargs='?')

    def parse_args(self,args):
        res = self.parser.parse_args(args[1:])
        return res

    def print_help(self):
        self.parser.print_help()
